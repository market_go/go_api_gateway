package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/market_go/go_api_gateway/config"
	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/genproto/organization_service"
	"gitlab.com/market_go/go_api_gateway/genproto/product_service"
	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
)

type ServiceManagerI interface {
	// Organization Services
	BranchService() organization_service.BranchServiceClient
	MagazineService() organization_service.MagazineServiceClient
	StaffService() organization_service.StaffServiceClient
	ShipperService() organization_service.ShipperServiceClient

	// Product Services
	BrandService() product_service.BrandServiceClient
	CategoryService() product_service.CategoryServiceClient
	ProductService() product_service.ProductServiceClient

	// Storage Services
	IncomeProductService() storage_service.IncomeProductServiceClient
	IncomeService() storage_service.IncomeServiceClient
	LeftService() storage_service.LeftServiceClient

	// Cashbox Services
	ShiftService() cashbox_service.ShiftServiceClient
	SaleService() cashbox_service.SaleServiceClient
	SaleProductService() cashbox_service.SaleProductServiceClient
	TransactionService() cashbox_service.TransactionServiceClient
	PaymentService() cashbox_service.PaymentServiceClient
}

type grpcClients struct {
	// Organization Services
	branchService   organization_service.BranchServiceClient
	magazineService organization_service.MagazineServiceClient
	staffService    organization_service.StaffServiceClient
	shipperService  organization_service.ShipperServiceClient

	// Product Services
	brandService    product_service.BrandServiceClient
	categoryService product_service.CategoryServiceClient
	productService  product_service.ProductServiceClient

	// Storage Services
	incomeproductService storage_service.IncomeProductServiceClient
	incomeService        storage_service.IncomeServiceClient
	leftServicr          storage_service.LeftServiceClient

	// Cashbox Services
	shiftService       cashbox_service.ShiftServiceClient
	saleService        cashbox_service.SaleServiceClient
	saleproducrService cashbox_service.SaleProductServiceClient
	transactionService cashbox_service.TransactionServiceClient
	paymentService     cashbox_service.PaymentServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Organization Microservice connection
	connOrganizationService, err := grpc.Dial(
		cfg.OrganizationServiceHost+cfg.OrganizationGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Product Microservice connection
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Storage Microservice connection
	connStorageService, err := grpc.Dial(
		cfg.StorageServiceHost+cfg.StorageGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	// Cashbox Microservice connection
	connCashboxService, err := grpc.Dial(
		cfg.CashboxServiceHost+cfg.CashboxGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Organization Service
		branchService:   organization_service.NewBranchServiceClient(connOrganizationService),
		magazineService: organization_service.NewMagazineServiceClient(connOrganizationService),
		staffService:    organization_service.NewStaffServiceClient(connOrganizationService),
		shipperService:  organization_service.NewShipperServiceClient(connOrganizationService),
		// Product Service
		brandService:    product_service.NewBrandServiceClient(connProductService),
		categoryService: product_service.NewCategoryServiceClient(connProductService),
		productService:  product_service.NewProductServiceClient(connProductService),
		// Storage Service
		incomeproductService: storage_service.NewIncomeProductServiceClient(connStorageService),
		incomeService:        storage_service.NewIncomeServiceClient(connStorageService),
		leftServicr:          storage_service.NewLeftServiceClient(connStorageService),
		// Cashbox Service
		shiftService:       cashbox_service.NewShiftServiceClient(connCashboxService),
		saleService:        cashbox_service.NewSaleServiceClient(connCashboxService),
		saleproducrService: cashbox_service.NewSaleProductServiceClient(connCashboxService),
		transactionService: cashbox_service.NewTransactionServiceClient(connCashboxService),
		paymentService:     cashbox_service.NewPaymentServiceClient(connCashboxService),
	}, nil
}

// Organization Services

func (g *grpcClients) BranchService() organization_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) MagazineService() organization_service.MagazineServiceClient {
	return g.magazineService
}

func (g *grpcClients) StaffService() organization_service.StaffServiceClient {
	return g.staffService
}

func (g *grpcClients) ShipperService() organization_service.ShipperServiceClient {
	return g.shipperService
}

// Product Services

func (g *grpcClients) BrandService() product_service.BrandServiceClient {
	return g.brandService
}

func (g *grpcClients) CategoryService() product_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}

// Storage Services

func (g *grpcClients) IncomeProductService() storage_service.IncomeProductServiceClient {
	return g.incomeproductService
}

func (g *grpcClients) IncomeService() storage_service.IncomeServiceClient {
	return g.incomeService
}

func (g *grpcClients) LeftService() storage_service.LeftServiceClient {
	return g.leftServicr
}

// Cashbox Services

func (g *grpcClients) ShiftService() cashbox_service.ShiftServiceClient {
	return g.shiftService
}

func (g *grpcClients) SaleService() cashbox_service.SaleServiceClient {
	return g.saleService
}

func (g *grpcClients) SaleProductService() cashbox_service.SaleProductServiceClient {
	return g.saleproducrService
}

func (g *grpcClients) TransactionService() cashbox_service.TransactionServiceClient {
	return g.transactionService
}

func (g *grpcClients) PaymentService() cashbox_service.PaymentServiceClient {
	return g.paymentService
}
