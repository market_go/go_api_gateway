package config

import (
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"

	TimeExpiredAt = time.Hour * 720
)

type Config struct {
	ServiceName string
	Environment string // debug, test, release
	Version     string

	ServiceHost string
	HTTPPort    string
	HTTPScheme  string
	Domain      string

	DefaultOffset string
	DefaultLimit  string

	// market_go_organization_service host and port for connection to service
	OrganizationServiceHost string
	OrganizationGRPCPort    string

	// market_go_product_service host and port for connection to service
	ProductServiceHost string
	ProductGRPCPort    string

	// market_go_storage_service host and port for connection to service
	StorageServiceHost string
	StorageGRPCPort    string

	// market_go_cashbox_service host and port for connection to service
	CashboxServiceHost string
	CashboxGRPCPort    string

	PostgresMaxConnections int32

	SecretKey string
}

// Load ...
func Load() Config {
	if err := godotenv.Load("./.env"); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "test_service"))
	config.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", ReleaseMode))
	config.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))

	config.ServiceHost = cast.ToString(getOrReturnDefaultValue("SERVICE_HOST", "localhost"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8001"))
	config.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_Scheme", "http"))
	config.Domain = cast.ToString(getOrReturnDefaultValue("DOMAIN", "localhost:8001"))

	config.DefaultOffset = cast.ToString(getOrReturnDefaultValue("OFFSET", "0"))
	config.DefaultLimit = cast.ToString(getOrReturnDefaultValue("LIMIT", "10"))

	config.OrganizationServiceHost = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_SERVICE_HOST", "localhost"))
	config.OrganizationGRPCPort = cast.ToString(getOrReturnDefaultValue("ORGANIZATION_GRPC_PORT", ":1111"))

	config.ProductServiceHost = cast.ToString(getOrReturnDefaultValue("PRODUCT_SERVICE_HOST", "localhost"))
	config.ProductGRPCPort = cast.ToString(getOrReturnDefaultValue("PRODUCT_GRPC_PORT", ":1111"))

	config.StorageServiceHost = cast.ToString(getOrReturnDefaultValue("STORAGE_SERVICE_HOST", "localhost"))
	config.StorageGRPCPort = cast.ToString(getOrReturnDefaultValue("STORAGE_GRPC_PORT", ":1111"))

	config.CashboxServiceHost = cast.ToString(getOrReturnDefaultValue("CASHBOX_SERVICE_HOST", "localhost"))
	config.CashboxGRPCPort = cast.ToString(getOrReturnDefaultValue("CASHBOX_GRPC_PORT", ":1111"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "car_rent"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
