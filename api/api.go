package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"gitlab.com/market_go/go_api_gateway/api/docs"
	"gitlab.com/market_go/go_api_gateway/api/handler"
	"gitlab.com/market_go/go_api_gateway/config"
)

func SetUpApi(r *gin.Engine, h *handler.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(500))
	// r.Use(h.AuthMiddleware())

	// Auth Api
	r.POST("/login", h.Login)
	r.POST("/register", h.Register)

	// Organization Services Api
	// Product api

	r.POST("/branch", h.AuthMiddleware(), h.BranchCreate)
	r.GET("/branch/:id", h.AuthMiddleware(), h.BranchGetById)
	r.GET("branch", h.AuthMiddleware(), h.BranchGetList)
	r.PUT("/branch/:id", h.AuthMiddleware(), h.BranchUpdate)
	r.DELETE("/branch/:id", h.AuthMiddleware(), h.BranchDelete)

	// Magazine Api
	r.POST("/magazine", h.AuthMiddleware(), h.MagazineCreate)
	r.GET("/magazine/:id", h.AuthMiddleware(), h.MagazineGetById)
	r.GET("magazine", h.AuthMiddleware(), h.MagazineGetList)
	r.PUT("/magazine/:id", h.AuthMiddleware(), h.MagazineUpdate)
	r.DELETE("/magazine/:id", h.AuthMiddleware(), h.MagazineDelete)

	// Staff Api
	r.POST("/staff", h.AuthMiddleware(), h.StaffCreate)
	r.GET("/staff/:id", h.AuthMiddleware(), h.StaffGetById)
	r.GET("staff", h.AuthMiddleware(), h.StaffGetList)
	r.PUT("/staff/:id", h.AuthMiddleware(), h.StaffUpdate)
	r.DELETE("/staff/:id", h.AuthMiddleware(), h.StaffDelete)

	// Shipper api
	r.POST("/shipper", h.AuthMiddleware(), h.ShipperCreate)
	r.GET("/shipper/:id", h.AuthMiddleware(), h.ShipperGetById)
	r.GET("shipper", h.AuthMiddleware(), h.ShipperGetList)
	r.PUT("/shipper/:id", h.AuthMiddleware(), h.ShipperUpdate)
	r.DELETE("/shipper/:id", h.AuthMiddleware(), h.ShipperDelete)

	// Product Services Api
	// Brand api
	r.POST("/brand", h.AuthMiddleware(), h.BrandCreate)
	r.GET("/brand/:id", h.AuthMiddleware(), h.BrandGetById)
	r.GET("brand", h.AuthMiddleware(), h.BrandGetList)
	r.PUT("/brand/:id", h.AuthMiddleware(), h.BrandUpdate)
	r.DELETE("/brand/:id", h.AuthMiddleware(), h.BrandDelete)

	// Category api
	r.POST("/category", h.AuthMiddleware(), h.CategoryCreate)
	r.GET("/category/:id", h.AuthMiddleware(), h.CategoryGetById)
	r.GET("category", h.AuthMiddleware(), h.CategoryGetList)
	r.PUT("/category/:id", h.AuthMiddleware(), h.CategoryUpdate)
	r.DELETE("/category/:id", h.AuthMiddleware(), h.CategoryDelete)

	// Product api
	r.POST("/product", h.AuthMiddleware(), h.ProductCreate)
	r.GET("/product/:id", h.AuthMiddleware(), h.ProductGetById)
	r.GET("product", h.AuthMiddleware(), h.ProductGetList)
	r.PUT("/product/:id", h.AuthMiddleware(), h.ProductUpdate)
	r.DELETE("/product/:id", h.AuthMiddleware(), h.ProductDelete)

	// Storage Services Api
	// Income-Product api
	r.POST("/income-product", h.AuthMiddleware(), h.IncomeProductCreate)
	r.GET("/income-product/:id", h.AuthMiddleware(), h.IncomeProductGetById)
	r.GET("income-product", h.AuthMiddleware(), h.IncomeProductGetList)
	r.PUT("/income-product/:id", h.AuthMiddleware(), h.IncomeProductUpdate)
	r.DELETE("/income-product/:id", h.AuthMiddleware(), h.IncomeProductDelete)

	// Income api
	r.POST("/income", h.AuthMiddleware(), h.IncomeCreate)
	r.GET("/income/:id", h.AuthMiddleware(), h.IncomeGetById)
	r.GET("income", h.AuthMiddleware(), h.IncomeGetList)
	r.PUT("/income/:id", h.AuthMiddleware(), h.IncomeUpdate)
	r.DELETE("/income/:id", h.AuthMiddleware(), h.IncomeDelete)

	// Left api
	r.POST("/left", h.AuthMiddleware(), h.LeftCreate)
	r.GET("/left/:id", h.AuthMiddleware(), h.LeftGetById)
	r.GET("left", h.AuthMiddleware(), h.LeftGetList)
	r.PUT("/left/:id", h.AuthMiddleware(), h.LeftUpdate)
	r.DELETE("/left/:id", h.AuthMiddleware(), h.LeftDelete)

	// Cashbox Services Api
	// Shift api
	r.POST("/shift", h.AuthMiddleware(), h.ShiftCreate)
	r.GET("/shift/:id", h.AuthMiddleware(), h.ShiftGetById)
	r.GET("shift", h.AuthMiddleware(), h.ShiftGetList)
	r.PUT("/shift/:id", h.AuthMiddleware(), h.ShiftUpdate)
	r.DELETE("/shift/:id", h.AuthMiddleware(), h.ShiftDelete)

	// Sale api
	r.POST("/sale", h.AuthMiddleware(), h.SaleCreate)
	r.GET("/sale/:id", h.AuthMiddleware(), h.SaleGetById)
	r.GET("sale", h.AuthMiddleware(), h.SaleGetList)
	r.PUT("/sale/:id", h.AuthMiddleware(), h.SaleUpdate)
	r.DELETE("/sale/:id", h.AuthMiddleware(), h.SaleDelete)

	// Sale-Product api
	r.POST("/sale-product", h.AuthMiddleware(), h.SaleProductCreate)
	r.GET("/sale-product/:id", h.AuthMiddleware(), h.SaleProductGetById)
	r.GET("sale-product", h.AuthMiddleware(), h.SaleProductGetList)
	r.PUT("/sale-product/:id", h.AuthMiddleware(), h.SaleProductUpdate)
	r.DELETE("/sale-product/:id", h.AuthMiddleware(), h.SaleProductDelete)

	// Transaction api
	r.POST("/transaction", h.AuthMiddleware(), h.TransactionCreate)
	r.GET("/transaction/:id", h.AuthMiddleware(), h.TransactionGetById)
	r.GET("transaction", h.AuthMiddleware(), h.TransactionGetList)
	r.PUT("/transaction/:id", h.AuthMiddleware(), h.TransactionUpdate)
	r.DELETE("/transaction/:id", h.AuthMiddleware(), h.TransactionDelete)

	// Payment api
	r.POST("/payment", h.AuthMiddleware(), h.PaymentCreate)
	r.GET("/payment/:id", h.AuthMiddleware(), h.PaymentGetById)
	r.GET("payment", h.AuthMiddleware(), h.PaymentGetList)
	r.PUT("/payment/:id", h.AuthMiddleware(), h.PaymentUpdate)
	r.DELETE("/payment/:id", h.AuthMiddleware(), h.PaymentDelete)

	// Make Income - (Сделать Приход)
	r.POST("/make-income", h.AuthMiddleware(), h.MakeIncome)

	// Make Sale - (Сделать Продажу)
	r.POST("/make-sale/:id", h.AuthMiddleware(), h.MakeSale)

	// Scan Product - (Сканировать Продукту)
	r.POST("/scan-product", h.AuthMiddleware(), h.ScanProduct)

	// Open Shift - (Открыть Смену)
	r.POST("/open-shift", h.AuthMiddleware(), h.OpenShift)

	// Close Shift - (Закрыть Смену)
	r.POST("close-shift/:id", h.AuthMiddleware(), h.CloseShift)

	url := ginSwagger.URL("swagger/doc.json")
	r.GET("swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
