package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/organization_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create magazine godoc
// @ID create_magazine
// @Router /magazine [POST]
// @Summary Create Magazine
// @Description Create Magazine
// @Tags Magazine
// @Accept json
// @Procedure json
// @Param Magazine body organization_service.MagazineCreate true "CreateMagazineRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MagazineCreate(ctx *gin.Context) {
	var Magazine organization_service.MagazineCreate

	err := ctx.ShouldBind(&Magazine)
	if err != nil {
		h.handlerResponse(ctx, "CreateMagazine", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.MagazineService().Create(ctx, &Magazine)
	if err != nil {
		h.handlerResponse(ctx, "MagazineService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Magazine resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID magazine godoc
// @ID get_by_id_magazine
// @Router /magazine/{id} [GET]
// @Summary Get By ID Magazine
// @Description Get By ID Magazine
// @Tags Magazine
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MagazineGetById(ctx *gin.Context) {
	MagazineId := ctx.Param("id")

	if !helper.IsValidUUID(MagazineId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.MagazineService().GetById(ctx, &organization_service.MagazinePrimaryKey{Id: MagazineId})
	if err != nil {
		h.handlerResponse(ctx, "MagazineService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Magazine resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList magazine godoc
// @ID get_list_magazine
// @Router /magazine [GET]
// @Summary Get List Magazine
// @Description Get List Magazine
// @Tags Magazine
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MagazineGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Magazine offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Magazine limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.MagazineService().GetList(ctx.Request.Context(), &organization_service.MagazineGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "MagazineService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Magazine resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update magazine godoc
// @ID update_magazine
// @Router /magazine/{id} [PUT]
// @Summary Update Magazine
// @Description Update Magazine
// @Tags Magazine
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Magazine body organization_service.MagazineUpdate true "UpdateMagazineRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MagazineUpdate(ctx *gin.Context) {
	var (
		id             string = ctx.Param("id")
		Magazineupdate organization_service.MagazineUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Magazineupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Magazine should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Magazineupdate.Id = id
	resp, err := h.services.MagazineService().Update(ctx.Request.Context(), &Magazineupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.MagazineService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Magazine resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete magazine godoc
// @ID delete_magazine
// @Router /magazine/{id} [DELETE]
// @Summary Delete Magazine
// @Description Delete Magazine
// @Tags Magazine
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MagazineDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.MagazineService().Delete(c.Request.Context(), &organization_service.MagazinePrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.MagazineService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Magazine resposne", http.StatusNoContent, resp)
}
