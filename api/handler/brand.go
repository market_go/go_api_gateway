package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/product_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create brand godoc
// @ID create-brand
// @Router /brand [POST]
// @Summary Create Brand
// @Description Create Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param Brand body product_service.BrandCreate true "CreateBrandRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandCreate(ctx *gin.Context) {
	var Brand product_service.BrandCreate

	err := ctx.ShouldBind(&Brand)
	if err != nil {
		h.handlerResponse(ctx, "CreateBrand", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.BrandService().Create(ctx, &Brand)
	if err != nil {
		h.handlerResponse(ctx, "BrandService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Brand resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID brand godoc
// @ID get-by-id-brand
// @Router /brand/{id} [GET]
// @Summary Get By ID Brand
// @Description Get By ID Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandGetById(ctx *gin.Context) {
	BrandId := ctx.Param("id")

	if !helper.IsValidUUID(BrandId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.BrandService().GetById(ctx, &product_service.BrandPrimaryKey{Id: BrandId})
	if err != nil {
		h.handlerResponse(ctx, "BrandService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Brand resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList brand godoc
// @ID get-list-brand
// @Router /brand [GET]
// @Summary Get List Brand
// @Description Get List Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Brand offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Brand limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.BrandService().GetList(ctx.Request.Context(), &product_service.BrandGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "BrandService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Brand resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update brand godoc
// @ID update-brand
// @Router /brand/{id} [PUT]
// @Summary Update Brand
// @Description Update Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Brand body product_service.BrandUpdate true "UpdateBrandRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandUpdate(ctx *gin.Context) {
	var (
		id          string = ctx.Param("id")
		Brandupdate product_service.BrandUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Brandupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Brand should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Brandupdate.Id = id
	resp, err := h.services.BrandService().Update(ctx.Request.Context(), &Brandupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.BrandService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Brand resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete brand godoc
// @ID delete-brand
// @Router /brand/{id} [DELETE]
// @Summary Delete Brand
// @Description Delete Brand
// @Tags Brand
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) BrandDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.BrandService().Delete(c.Request.Context(), &product_service.BrandPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.BrandService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Brand resposne", http.StatusNoContent, resp)
}
