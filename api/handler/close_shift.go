package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CloseShift godoc
// @ID CloseShift
// @Router /close-shift/{id} [POST]
// @Summary CloseShift
// @Description CloseShift
// @Tags CloseShift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) CloseShift(ctx *gin.Context) {
	ShiftId := ctx.Param("id")

	if !helper.IsValidUUID(ShiftId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}
	// Here Get Shift To update Status to Closed
	resp, err := h.services.ShiftService().GetById(ctx, &cashbox_service.ShiftPrimaryKey{Id: ShiftId})
	if err != nil {
		h.handlerResponse(ctx, "CloseShift=>h.services.ShiftService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	// Here Status Shift Updated To CLosed
	_, err = h.services.ShiftService().Update(ctx, &cashbox_service.ShiftUpdate{
		Id:       resp.Id,
		Branch:   resp.Branch,
		Staff:    resp.Staff,
		Magazine: resp.Magazine,
		Status:   "closed",
	})
	if err != nil {
		h.handlerResponse(ctx, "CloseShift=>h.services.h.services.ShiftService().Update", http.StatusBadRequest, err.Error())
		return
	}
	h.handlerResponse(ctx, "Close Shift response", http.StatusNoContent, "")
}
