package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
)

// @Security ApiKeyAuth
// Create MakeSale godoc
// @ID MakeSale
// @Router /make-sale/{id} [POST]
// @Summary MakeSale
// @Description Make ale
// @Tags MakeSale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h Handler) MakeSale(ctx *gin.Context) {
	MakeSaleId := ctx.Param("id")

	// Here Get Sale to Update Status to Finished
	sale, err := h.services.SaleService().GetById(ctx, &cashbox_service.SalePrimaryKey{Id: MakeSaleId})

	if err != nil {
		h.handlerResponse(ctx, "SaleService=>h.services.SaleService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	// Here Sale status Updated to finished
	_, err = h.services.SaleService().Update(ctx, &cashbox_service.SaleUpdate{
		Id:       sale.Id,
		ShiftId:  sale.ShiftId,
		Branch:   sale.Branch,
		Magazine: sale.Magazine,
		Staff:    sale.Staff,
		Status:   "finished",
	})

	if err != nil {
		h.handlerResponse(ctx, "SaleService=>h.services.SaleService().Update", http.StatusBadRequest, err.Error())
		return
	}
	// Here Get Sale Product to Update Left Table
	sale_product, err := h.services.SaleProductService().GetList(ctx, &cashbox_service.SaleProductGetListRequest{
		SearchBySaleId: sale.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService=>h.services.SaleProductService().GetList", http.StatusBadRequest, err.Error())
		return
	}

	for _, value := range sale_product.SaleProductes {
		// Here Get Left Table to Update quantity
		left, err := h.services.LeftService().GetById(ctx, &storage_service.LeftPrimaryKey{
			ProductId: value.ProductName,
		})
		if err != nil {
			h.handlerResponse(ctx, "SaleProductService=>h.services.LeftService().GetById", http.StatusBadRequest, err.Error())
			return
		}
		// Here Update Left Table quantity
		left.Quantity -= value.Quantity
		_, err = h.services.LeftService().Update(ctx, &storage_service.LeftUpdate{
			Id:          left.Id,
			Branch:      left.Branch,
			Brand:       left.Brand,
			Category:    left.Category,
			ProductName: left.ProductName,
			Barcode:     left.Barcode,
			PriceIncome: left.PriceIncome,
			Quantity:    left.Quantity,
		})
		if err != nil {
			h.handlerResponse(ctx, "SaleProductService=>h.services.LeftService().Update", http.StatusBadRequest, err.Error())
			return
		}
	}

	// Here Get Payment To Update Transaction
	payment, err := h.services.PaymentService().GetById(ctx, &cashbox_service.PaymentPrimaryKey{
		SaleId: sale.Id,
	})

	if err != nil {
		h.handlerResponse(ctx, "PaymentService=>h.services.PaymentService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	// Here Get Shift to Update Transaction
	shift, err := h.services.ShiftService().GetById(ctx, &cashbox_service.ShiftPrimaryKey{Id: sale.ShiftId})

	if err != nil {
		h.handlerResponse(ctx, "ShiftService=>h.services.ShiftService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	// Here Get Transaction To Update
	transaction, err := h.services.TransactionService().GetList(ctx, &cashbox_service.TransactionGetListRequest{
		SearchShiftId: shift.Id,
	})

	if err != nil {
		h.handlerResponse(ctx, "ShiftService=>h.services.TransactionService().GetList", http.StatusBadRequest, err.Error())
		return
	}

	// Here Update Transaction
	_, err = h.services.TransactionService().Update(ctx, &cashbox_service.TransactionUpdate{
		Id:       transaction.Transactions[0].Id,
		Cash:     payment.Cash,
		Uzcard:   payment.Uzcard,
		Payme:    payment.Payme,
		Click:    payment.Click,
		Humo:     payment.Humo,
		Uzum:     payment.Uzum,
		TotalSum: payment.TotalSum,
		ShiftId:  shift.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService=>h.services.TransactionService().Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "MakeSale Response", http.StatusNoContent, "")
}
