package handler

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/market_go/go_api_gateway/genproto/organization_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// Login godoc
// @ID login
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Procedure json
// @Param login body organization_service.StaffLogin true "LoginRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Login(ctx *gin.Context) {
	var login organization_service.StaffLogin

	err := ctx.ShouldBindJSON(&login) // parse req body to given type struct
	if err != nil {
		h.handlerResponse(ctx, "create staff", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.StaffService().GetById(ctx, &organization_service.StaffPrimaryKey{Login: login.Login})
	if err != nil {
		if err.Error() == "no rows in result set" {
			h.handlerResponse(ctx, "staff does not exist", http.StatusBadRequest, "User does not exist")
			return
		}
		h.handlerResponse(ctx, "storage.staff.getByID", http.StatusInternalServerError, err.Error())
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(resp.Password), []byte(login.Password))
	if err != nil {
		h.handlerResponse(ctx, "Compare Password", http.StatusInternalServerError, err.Error())
		return
	}

	token, err := helper.GenerateJWT(map[string]interface{}{
		"user_id": resp.Id,
	}, time.Hour*360, h.cfg.SecretKey)
	if err != nil {
		h.handlerResponse(ctx, "Generate JWT", http.StatusInternalServerError, err.Error())
		return
	}

	ctx.SetCookie("SESSTOKEN", token, 1000, "/", "localhost", false, true)
	h.handlerResponse(ctx, "token", http.StatusAccepted, token)
}

// Register godoc
// @ID Register
// @Router /register [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Procedure json
// @Param register body organization_service.StaffCreate true "RegisterRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) Register(ctx *gin.Context) {

	var createStaff organization_service.StaffCreate

	err := ctx.ShouldBindJSON(&createStaff)
	if err != nil {
		h.handlerResponse(ctx, "error user should bind json", http.StatusBadRequest, err.Error())
		return
	}

	if len(createStaff.Password) < 7 {
		h.handlerResponse(ctx, "Password should inculude more than 7 elements", http.StatusBadRequest, errors.New("!Password len should inculude more than 8 elements"))
		return
	}

	_, err = h.services.StaffService().GetById(ctx, &organization_service.StaffPrimaryKey{
		Login: createStaff.Login,
	})
	fmt.Println(err.Error())
	if err != nil {
		if err.Error() == "rpc error: code = InvalidArgument desc = no rows in result set" {
			log.Println("AAAAAAA", err.Error())
			resp, err := h.services.StaffService().Create(ctx, &createStaff)
			if err != nil {
				h.handlerResponse(ctx, "Register=>h.services.StaffService().Create", http.StatusInternalServerError, err.Error())
				return
			}
			h.handlerResponse(ctx, "Register Response", http.StatusCreated, resp)
			return
		} else {
			fmt.Println(err.Error())
			h.handlerResponse(ctx, "Register=>h.services.StaffService().GetById", http.StatusBadRequest, err.Error())
			return
		}
	} else if err == nil {
		h.handlerResponse(ctx, "User already exist", http.StatusBadRequest, nil)
		return
	}

}
