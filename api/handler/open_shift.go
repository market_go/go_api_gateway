package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
)

// @Security ApiKeyAuth
// OpenShift godoc
// @ID OpenShift
// @Router /open-shift [POST]
// @Summary OpenShift
// @Description OpenShift
// @Tags OpenShift
// @Accept json
// @Procedure json
// @Param OpenShift body cashbox_service.ShiftCreate true "OpenShiftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) OpenShift(ctx *gin.Context) {
	var Shift cashbox_service.ShiftCreate

	err := ctx.ShouldBind(&Shift)
	if err != nil {
		h.handlerResponse(ctx, "OpenShift", http.StatusBadRequest, err.Error())
		return
	}
	// Here Check Shift Opened on this Branch or closed
	branch, err := h.services.ShiftService().GetList(ctx, &cashbox_service.ShiftGetListRequest{
		SerchByBranchId: Shift.Branch,
	})

	if err != nil {
		h.handlerResponse(ctx, "OpenShift=>h.services.ShiftService().GetList", http.StatusBadRequest, err.Error())
		return
	}
	for _, value := range branch.Shiftes {
		if value.Status == "opened" {
			h.handlerResponse(ctx, "Find Opened Shift", http.StatusBadRequest, "У этого филиала уже есть открытая смена. Пожалуйста, сначала закройте его!")
			return
		}
	}

	// Here Create Shift
	resp, err := h.services.ShiftService().Create(ctx, &Shift)
	if err != nil {
		h.handlerResponse(ctx, "OpenShift=>h.services.ShiftService().Create", http.StatusBadRequest, err.Error())
		return
	}
	// Here Create Transaction
	_, err = h.services.TransactionService().Create(ctx, &cashbox_service.TransactionCreate{
		Cash:     0,
		Uzcard:   0,
		Payme:    0,
		Click:    0,
		Humo:     0,
		Uzum:     0,
		TotalSum: 0,
		ShiftId:  resp.Id,
	})
	if err != nil {
		h.handlerResponse(ctx, "OpenShift=>h.services.TransactionService().Create", http.StatusBadRequest, err.Error())
		return
	}

	// Here Update Shift To add Transaction Id
	// shift, err := h.services.ShiftService().Update(ctx, &cashbox_service.ShiftUpdate{
	// 	Id:            resp.Id,
	// 	Branch:        resp.Branch,
	// 	Staff:         resp.Staff,
	// 	Magazine:      resp.Magazine,
	// 	Status:        resp.Status,
	// 	TransactionId: transaction.Id,
	// })
	// if err != nil {
	// 	h.handlerResponse(ctx, "OpenShift=>h.services.ShiftService().Update", http.StatusBadRequest, err.Error())
	// 	return
	// }

	h.handlerResponse(ctx, "OpenShift resposne", http.StatusOK, resp)
}
