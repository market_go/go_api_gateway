package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create transaction godoc
// @ID create-transaction
// @Router /transaction [POST]
// @Summary Create Transaction
// @Description Create Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param Transaction body cashbox_service.TransactionCreate true "CreateTransactionRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionCreate(ctx *gin.Context) {
	var Transaction cashbox_service.TransactionCreate

	err := ctx.ShouldBind(&Transaction)
	if err != nil {
		h.handlerResponse(ctx, "CreateTransaction", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.TransactionService().Create(ctx, &Transaction)
	if err != nil {
		h.handlerResponse(ctx, "TransactionService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Transaction resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID transaction godoc
// @ID get-by-id-transaction
// @Router /transaction/{id} [GET]
// @Summary Get By ID Transaction
// @Description Get By ID Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionGetById(ctx *gin.Context) {
	TransactionId := ctx.Param("id")

	if !helper.IsValidUUID(TransactionId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.TransactionService().GetById(ctx, &cashbox_service.TransactionPrimaryKey{Id: TransactionId})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Transaction resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList transaction godoc
// @ID get-list-transaction
// @Router /transaction [GET]
// @Summary Get List Transaction
// @Description Get List Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-shift-id query string false "search-shift-id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Transaction offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Transaction limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.TransactionService().GetList(ctx.Request.Context(), &cashbox_service.TransactionGetListRequest{
		Offset:        int64(offset),
		Limit:         int64(limit),
		SearchShiftId: ctx.Query("search-shift-id"),
	})
	if err != nil {
		h.handlerResponse(ctx, "TransactionService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Transaction resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update transaction godoc
// @ID update-transaction
// @Router /transaction/{id} [PUT]
// @Summary Update Transaction
// @Description Update Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Transaction body cashbox_service.TransactionUpdate true "UpdateTransactionRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionUpdate(ctx *gin.Context) {
	var (
		id                string = ctx.Param("id")
		Transactionupdate cashbox_service.TransactionUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Transactionupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Transaction should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Transactionupdate.Id = id
	resp, err := h.services.TransactionService().Update(ctx.Request.Context(), &Transactionupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.TransactionService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Transaction resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete transaction godoc
// @ID delete-transaction
// @Router /transaction/{id} [DELETE]
// @Summary Delete Transaction
// @Description Delete Transaction
// @Tags Transaction
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) TransactionDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.TransactionService().Delete(c.Request.Context(), &cashbox_service.TransactionPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.TransactionService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Transaction resposne", http.StatusNoContent, resp)
}
