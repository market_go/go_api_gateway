package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/product_service"
	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create income-product godoc
// @ID create-income-product
// @Router /income-product [POST]
// @Summary Create IncomeProduct
// @Description Create IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Procedure json
// @Param IncomeProduct body storage_service.IncomeProductCreate true "CreateIncomeProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeProductCreate(ctx *gin.Context) {
	var IncomeProduct storage_service.IncomeProductCreate

	err := ctx.ShouldBind(&IncomeProduct)
	if err != nil {
		h.handlerResponse(ctx, "CreateIncomeProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeProductService().Create(ctx, &IncomeProduct)
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Create", http.StatusBadRequest, err.Error())
		return
	}

	// Get Brand Name
	brand, err := h.services.BrandService().GetById(ctx, &product_service.BrandPrimaryKey{
		Id: resp.Brand,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Create=>h.services.BrandService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.Brand = brand.Name
	// Get Category Name
	category, err := h.services.CategoryService().GetById(ctx, &product_service.CategoryPrimaryKey{
		Id: resp.Category,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Create=>h.services.CategoryService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.Category = category.Name
	// Get Product Name
	product, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{
		Id: resp.ProductName,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Create=>h.services.ProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.ProductName = product.Name

	h.handlerResponse(ctx, "create IncomeProduct resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID income-product godoc
// @ID get-by-id-income-product
// @Router /income-product/{id} [GET]
// @Summary Get By ID IncomeProduct
// @Description Get By ID IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeProductGetById(ctx *gin.Context) {
	IncomeProductId := ctx.Param("id")

	if !helper.IsValidUUID(IncomeProductId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.IncomeProductService().GetById(ctx, &storage_service.IncomeProductPrimaryKey{Id: IncomeProductId})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	// Get Brand Name
	brand, err := h.services.BrandService().GetById(ctx, &product_service.BrandPrimaryKey{
		Id: resp.Brand,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().GetById=>h.services.BrandService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.Brand = brand.Name
	// Get Category Name
	category, err := h.services.CategoryService().GetById(ctx, &product_service.CategoryPrimaryKey{
		Id: resp.Category,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().GetById=>h.services.CategoryService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.Category = category.Name
	// Get Product Name
	product, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{
		Id: resp.ProductName,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().GetById=>h.services.ProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.ProductName = product.Name

	h.handlerResponse(ctx, "get by id IncomeProduct resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList income-product godoc
// @ID get-list-income-product
// @Router /income-product [GET]
// @Summary Get List IncomeProduct
// @Description Get List IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-brand query string false "Поиск по Бренду"
// @Param search-by-category query string false "Поиск по Категортю"
// @Param search-by-barcode query string false "Поиск по Штрихкоду"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list IncomeProduct offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list IncomeProduct limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.IncomeProductService().GetList(ctx.Request.Context(), &storage_service.IncomeProductGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByCategory: ctx.Query("search-by-category"),
		SerachByBarcode:  ctx.Query("search-by-barcode"),
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	for index, value := range resp.IncomeProducts {
		// Get Brand Name
		brand, err := h.services.BrandService().GetById(ctx, &product_service.BrandPrimaryKey{
			Id: value.Brand,
		})
		if err != nil {
			h.handlerResponse(ctx, "IncomeProductService().GetList=>h.services.BrandService().GetById(", http.StatusInternalServerError, err.Error())
			return
		}
		resp.IncomeProducts[index].Brand = brand.Name
		// Get Category Name
		category, err := h.services.CategoryService().GetById(ctx, &product_service.CategoryPrimaryKey{
			Id: value.Category,
		})
		if err != nil {
			h.handlerResponse(ctx, "IncomeProductService().GetList=>h.services.CategoryService().GetById", http.StatusInternalServerError, err.Error())
			return
		}
		resp.IncomeProducts[index].Category = category.Name
		// Get Product Name
		product, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{
			Id: value.ProductName,
		})
		if err != nil {
			h.handlerResponse(ctx, "IncomeProductService().GetList=>h.services.ProductService().GetById", http.StatusInternalServerError, err.Error())
			return
		}
		resp.IncomeProducts[index].ProductName = product.Name
	}

	h.handlerResponse(ctx, "get list IncomeProduct resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update income-product godoc
// @ID update-income-product
// @Router /income-product/{id} [PUT]
// @Summary Update IncomeProduct
// @Description Update IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param IncomeProduct body storage_service.IncomeProductUpdate true "UpdateIncomeProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeProductUpdate(ctx *gin.Context) {
	var (
		id                  string = ctx.Param("id")
		IncomeProductupdate storage_service.IncomeProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&IncomeProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "error IncomeProduct should bind json", http.StatusBadRequest, err.Error())
		return
	}

	IncomeProductupdate.Id = id
	resp, err := h.services.IncomeProductService().Update(ctx.Request.Context(), &IncomeProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.IncomeProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	// Get Brand Name
	brand, err := h.services.BrandService().GetById(ctx, &product_service.BrandPrimaryKey{
		Id: resp.Brand,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Update=>h.services.BrandService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.Brand = brand.Name
	// Get Category Name
	category, err := h.services.CategoryService().GetById(ctx, &product_service.CategoryPrimaryKey{
		Id: resp.Category,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Update=>h.services.CategoryService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.Category = category.Name
	// Get Product Name
	product, err := h.services.ProductService().GetById(ctx, &product_service.ProductPrimaryKey{
		Id: resp.ProductName,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeProductService().Update=>h.services.ProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	resp.ProductName = product.Name

	h.handlerResponse(ctx, "create IncomeProduct resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete income-product godoc
// @ID delete-income-product
// @Router /income-product/{id} [DELETE]
// @Summary Delete IncomeProduct
// @Description Delete IncomeProduct
// @Tags IncomeProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeProductDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.IncomeProductService().Delete(c.Request.Context(), &storage_service.IncomeProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.IncomeProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create IncomeProduct resposne", http.StatusNoContent, resp)
}
