package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/organization_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create shipper godoc
// @ID create_shipper
// @Router /shipper [POST]
// @Summary Create Shipper
// @Description Create Shipper
// @Tags Shipper
// @Accept json
// @Procedure json
// @Param Shipper body organization_service.ShipperCreate true "CreateShipperRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShipperCreate(ctx *gin.Context) {
	var Shipper organization_service.ShipperCreate

	err := ctx.ShouldBind(&Shipper)
	if err != nil {
		h.handlerResponse(ctx, "CreateShipper", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShipperService().Create(ctx, &Shipper)
	if err != nil {
		h.handlerResponse(ctx, "ShipperService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Shipper resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID shipper godoc
// @ID get_by_id_shipper
// @Router /shipper/{id} [GET]
// @Summary Get By ID Shipper
// @Description Get By ID Shipper
// @Tags Shipper
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShipperGetById(ctx *gin.Context) {
	ShipperId := ctx.Param("id")

	if !helper.IsValidUUID(ShipperId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.ShipperService().GetById(ctx, &organization_service.ShipperPrimaryKey{Id: ShipperId})
	if err != nil {
		h.handlerResponse(ctx, "ShipperService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Shipper resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList shipper godoc
// @ID get_list_shipper
// @Router /shipper [GET]
// @Summary Get List Shipper
// @Description Get List Shipper
// @Tags Shipper
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search query string false "search"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShipperGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Shipper offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Shipper limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ShipperService().GetList(ctx.Request.Context(), &organization_service.ShipperGetListRequest{
		Offset: int64(offset),
		Limit:  int64(limit),
		Search: ctx.Query("search"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ShipperService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Shipper resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update shipper godoc
// @ID update_shipper
// @Router /shipper/{id} [PUT]
// @Summary Update Shipper
// @Description Update Shipper
// @Tags Shipper
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Shipper body organization_service.ShipperUpdate true "UpdateShipperRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShipperUpdate(ctx *gin.Context) {
	var (
		id            string = ctx.Param("id")
		Shipperupdate organization_service.ShipperUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Shipperupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Shipper should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Shipperupdate.Id = id
	resp, err := h.services.ShipperService().Update(ctx.Request.Context(), &Shipperupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ShipperService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Shipper resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete shipper godoc
// @ID delete_shipper
// @Router /shipper/{id} [DELETE]
// @Summary Delete Shipper
// @Description Delete Shipper
// @Tags Shipper
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShipperDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ShipperService().Delete(c.Request.Context(), &organization_service.ShipperPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ShipperService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Shipper resposne", http.StatusNoContent, resp)
}
