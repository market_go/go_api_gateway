package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create sale-product godoc
// @ID create-sale-product
// @Router /sale-product [POST]
// @Summary Create SaleProduct
// @Description Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param SaleProduct body cashbox_service.SaleProductCreate true "CreateSaleProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductCreate(ctx *gin.Context) {
	var SaleProduct cashbox_service.SaleProductCreate

	err := ctx.ShouldBind(&SaleProduct)
	if err != nil {
		h.handlerResponse(ctx, "CreateSaleProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.SaleProductService().Create(ctx, &SaleProduct)
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create SaleProduct resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID sale-product godoc
// @ID get-by-id-sale-product
// @Router /sale-product/{id} [GET]
// @Summary Get By ID SaleProduct
// @Description Get By ID SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductGetById(ctx *gin.Context) {
	SaleProductId := ctx.Param("id")

	if !helper.IsValidUUID(SaleProductId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.SaleProductService().GetById(ctx, &cashbox_service.SaleProductPrimaryKey{Id: SaleProductId})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Bsale-productrand resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList sale-product godoc
// @ID get-list-sale-product
// @Router /sale-product [GET]
// @Summary Get List SaleProduct
// @Description Get List SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-sale-id query string false "Поиск по Продажа Ид"
// @Param search-by-brand query string false "Поиск по Бренду"
// @Param search-by-barcode query string false "Поиск по Штрихкоду"
// @Param search-by-category query string false "Поиск по Категори"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list SaleProduct offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list SaleProduct limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SaleProductService().GetList(ctx.Request.Context(), &cashbox_service.SaleProductGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchBySaleId:   ctx.Query("search-by-sale-id"),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByBarcode:  ctx.Query("search-by-barcode"),
		SearchByCategory: ctx.Query("search-by-category"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleProductService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list SaleProduct resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update sale-product godoc
// @ID update-sale-product
// @Router /sale-product/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param SaleProduct body cashbox_service.SaleProductUpdate true "UpdateSaleProductRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductUpdate(ctx *gin.Context) {
	var (
		id                string = ctx.Param("id")
		SaleProductupdate cashbox_service.SaleProductUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&SaleProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "error SaleProduct should bind json", http.StatusBadRequest, err.Error())
		return
	}

	SaleProductupdate.Id = id
	resp, err := h.services.SaleProductService().Update(ctx.Request.Context(), &SaleProductupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SaleProductService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create SaleProduct resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete sale-product godoc
// @ID delete-sale-product
// @Router /sale-product/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleProductDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SaleProductService().Delete(c.Request.Context(), &cashbox_service.SaleProductPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SaleProductService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create SaleProduct resposne", http.StatusNoContent, resp)
}
