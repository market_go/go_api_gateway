package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/genproto/product_service"
	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
)

// @Security ApiKeyAuth
// ScanProduct godoc
// @ID ScanProduct
// @Router /scan-product [POST]
// @Summary ScanProduct
// @Description ScanProduct
// @Tags ScanProduct
// @Accept json
// @Procedure json
// @Param ScanProduct body cashbox_service.ScanProduct true "MakeIncomeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ScanProduct(ctx *gin.Context) {
	var ScanProduct cashbox_service.ScanProduct

	err := ctx.ShouldBind(&ScanProduct)
	if err != nil {
		h.handlerResponse(ctx, "ScanProduct", http.StatusBadRequest, err.Error())
		return
	}

	// Here Get SaleProduct to Udpdate or Create
	sale_product, err := h.services.SaleProductService().GetById(ctx, &cashbox_service.SaleProductPrimaryKey{
		Barcode: ScanProduct.Barcode,
	})
	if err != nil {
		if err.Error() == "no rows in result set" {
			// Here Get Product to Create SaleProduct
			product, err := h.services.ProductService().GetList(ctx, &product_service.ProductGetListRequest{
				SearchByBarcode: ScanProduct.Barcode,
			})
			if err != nil {
				h.handlerResponse(ctx, "ScanProduct=>h.services.ProductService().GetList", http.StatusBadRequest, err.Error())
				return
			}
			// Get Left to Create SaleProduct
			left, err := h.services.LeftService().GetById(ctx, &storage_service.LeftPrimaryKey{
				ProductId: product.Products[0].Id,
			})
			if err != nil {
				h.handlerResponse(ctx, "ScanProduct=>h.services.LeftService().GetById", http.StatusBadRequest, err.Error())
				return
			}

			// Here Create SaleProduct
			resp, err := h.services.SaleProductService().Create(ctx, &cashbox_service.SaleProductCreate{
				SaleId:       ScanProduct.SaleId,
				Brand:        left.Brand,
				Category:     left.Category,
				ProductName:  left.ProductName,
				Barcode:      left.Barcode,
				LeftQuantity: left.Quantity,
				Quantity:     1,
				Price:        left.PriceIncome,
				TotalPrice:   left.PriceIncome,
			})
			if err != nil {
				h.handlerResponse(ctx, "ScanProduct=>h.services.SaleProductService().Create", http.StatusBadRequest, err.Error())
				return
			}
			h.handlerResponse(ctx, "Scan Product resposne", http.StatusNoContent, resp)
			return
		} else {
			h.handlerResponse(ctx, "ScanProduct=>SaleProductService().GetById", http.StatusBadRequest, err.Error())
			return
		}
	}

	resp, err := h.services.SaleProductService().Update(ctx, &cashbox_service.SaleProductUpdate{
		Id:           sale_product.Id,
		SaleId:       sale_product.SaleId,
		Brand:        sale_product.Brand,
		Category:     sale_product.Category,
		ProductName:  sale_product.ProductName,
		Barcode:      sale_product.Barcode,
		LeftQuantity: sale_product.LeftQuantity,
		Quantity:     sale_product.Quantity + 1,
		Price:        sale_product.Price,
		TotalPrice:   sale_product.TotalPrice,
	})
	if err != nil {
		h.handlerResponse(ctx, "ScanProduct=>SaleProductService().Update", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "Do Income resposne", http.StatusNoContent, resp)
}
