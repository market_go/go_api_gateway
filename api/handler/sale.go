package handler

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/genproto/organization_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create sale godoc
// @ID create-sale
// @Router /sale [POST]
// @Summary Create Sale
// @Description Create Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param Sale body cashbox_service.SaleCreate true "CreateSaleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleCreate(ctx *gin.Context) {
	var Sale cashbox_service.SaleCreate

	err := ctx.ShouldBind(&Sale)
	if err != nil {
		h.handlerResponse(ctx, "CreateSale", http.StatusBadRequest, err.Error())
		return
	}

	// Here Check Shift Opened Or Closed
	shift, err := h.services.ShiftService().GetById(ctx, &cashbox_service.ShiftPrimaryKey{
		Id: Sale.ShiftId,
	})
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().GetById", http.StatusBadRequest, err.Error())
		return
	}
	fmt.Println(shift.Status)
	// Here Check Status Staff
	if shift.Status != "opened" {
		h.handlerResponse(ctx, "ShiftService().GetById", http.StatusBadRequest, errors.New("!У вас нет открытая смена. Пожалуйста откройте смену"))
		return
	}

	// Here Get Branch To Generate SaleId
	branch, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{
		Id: Sale.Branch,
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().Create=>h.services.BranchService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	Sale.SaleId = branch.Name

	resp, err := h.services.SaleService().Create(ctx, &Sale)
	if err != nil {
		h.handlerResponse(ctx, "SaleService().Create", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Sale resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID sale godoc
// @ID get-by-id-sale
// @Router /sale/{id} [GET]
// @Summary Get By ID Sale
// @Description Get By ID Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleGetById(ctx *gin.Context) {
	SaleId := ctx.Param("id")

	if !helper.IsValidUUID(SaleId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.SaleService().GetById(ctx, &cashbox_service.SalePrimaryKey{Id: SaleId})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Sale resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList sale godoc
// @ID get-list-sale
// @Router /sale [GET]
// @Summary Get List Sale
// @Description Get List Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-sale-id query string false "Поиск по Продажа Ид"
// @Param search-by-shift-id query string false "Поиск по Смена Ид"
// @Param search-by-branch query string false "Поиск по Филиалу"
// @Param search-by-staff query string false "Поиск по Работникам"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Sale offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Sale limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.SaleService().GetList(ctx.Request.Context(), &cashbox_service.SaleGetListRequest{
		Offset:          int64(offset),
		Limit:           int64(limit),
		SearchBySaleId:  ctx.Query("search-by-sale-id"),
		SearchByShiftId: ctx.Query("search-by-shift-id"),
		SearchByBranch:  ctx.Query("search-by-branch"),
		SearchByStaff:   ctx.Query("search-by-staff"),
	})
	if err != nil {
		h.handlerResponse(ctx, "SaleService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Sale resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update sale godoc
// @ID update-sale
// @Router /sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Sale body cashbox_service.SaleUpdate true "UpdateSaleRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleUpdate(ctx *gin.Context) {
	var (
		id         string = ctx.Param("id")
		Saleupdate cashbox_service.SaleUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Saleupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Sale should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Saleupdate.Id = id
	resp, err := h.services.SaleService().Update(ctx.Request.Context(), &Saleupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.SaleService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Sale resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete sale godoc
// @ID delete-sale
// @Router /sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) SaleDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.SaleService().Delete(c.Request.Context(), &cashbox_service.SalePrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.SaleService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Sale resposne", http.StatusNoContent, resp)
}
