package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/organization_service"
	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create income godoc
// @ID create-income
// @Router /income [POST]
// @Summary Create Income
// @Description Create Income
// @Tags Income
// @Accept json
// @Procedure json
// @Param Income body storage_service.IncomeCreate true "CreateIncomeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeCreate(ctx *gin.Context) {
	var Income storage_service.IncomeCreate

	err := ctx.ShouldBind(&Income)
	if err != nil {
		h.handlerResponse(ctx, "CreateIncome", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.IncomeService().Create(ctx, &Income)
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().Create", http.StatusBadRequest, err.Error())

		return
	}

	// Get Branch Name
	branch, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{
		Id: resp.Branch,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().Create=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
		return
	}
	resp.Branch = branch.Name
	// Get Shipper Name
	shipper, err := h.services.ShipperService().GetById(ctx, &organization_service.ShipperPrimaryKey{
		Id: resp.Shipper,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().Create=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
		return
	}
	resp.Shipper = shipper.Name

	h.handlerResponse(ctx, "create Income resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID income godoc
// @ID get-by-id-income
// @Router /income/{id} [GET]
// @Summary Get By ID Income
// @Description Get By ID Income
// @Tags Income
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeGetById(ctx *gin.Context) {
	IncomeId := ctx.Param("id")

	if !helper.IsValidUUID(IncomeId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.IncomeService().GetById(ctx, &storage_service.IncomePrimaryKey{Id: IncomeId})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	// Get Branch Name
	branch, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{
		Id: resp.Branch,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().GetById=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
		return
	}
	resp.Branch = branch.Name
	// Get Shipper Name
	shipper, err := h.services.ShipperService().GetById(ctx, &organization_service.ShipperPrimaryKey{
		Id: resp.Shipper,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().GetById=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
		return
	}
	resp.Shipper = shipper.Name

	h.handlerResponse(ctx, "get by id Income resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList income godoc
// @ID get-list-income
// @Router /income [GET]
// @Summary Get List Income
// @Description Get List Income
// @Tags Income
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-income-id query string false "Поиск по Приход-Ид"
// @Param search-by-branch query string false "Поиск по Филиалу"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Income offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Income limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.IncomeService().GetList(ctx.Request.Context(), &storage_service.IncomeGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByIncomeId: ctx.Query("search-by-income-id"),
		SearchByBranch:   ctx.Query("search-by-branch"),
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	for index, value := range resp.Incomes {
		// Get Branch Name
		branch, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{
			Id: value.Branch,
		})
		if err != nil {
			h.handlerResponse(ctx, "IncomeService().GetList=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
			return
		}
		resp.Incomes[index].Branch = branch.Name
		// Get Shipper Name
		shipper, err := h.services.ShipperService().GetById(ctx, &organization_service.ShipperPrimaryKey{
			Id: value.Shipper,
		})
		if err != nil {
			h.handlerResponse(ctx, "IncomeService().GetList=>h.services.ShipperService().GetById", http.StatusInternalServerError, err.Error())
			return
		}
		resp.Incomes[index].Shipper = shipper.Name
	}

	h.handlerResponse(ctx, "get list Income resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update income godoc
// @ID update-income
// @Router /income/{id} [PUT]
// @Summary Update Income
// @Description Update Income
// @Tags Income
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Income body storage_service.IncomeUpdate true "UpdateIncomeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeUpdate(ctx *gin.Context) {
	var (
		id           string = ctx.Param("id")
		Incomeupdate storage_service.IncomeUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Incomeupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Income should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Incomeupdate.Id = id
	resp, err := h.services.IncomeService().Update(ctx.Request.Context(), &Incomeupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.IncomeService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	// Get Branch Name
	branch, err := h.services.BranchService().GetById(ctx, &organization_service.BranchPrimaryKey{
		Id: resp.Branch,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().Update=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
		return
	}
	resp.Branch = branch.Name
	// Get Shipper Name
	shipper, err := h.services.ShipperService().GetById(ctx, &organization_service.ShipperPrimaryKey{
		Id: resp.Shipper,
	})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().Update=>h.services.BranchService().GetById", http.StatusInternalServerError, err.Error())
		return
	}
	resp.Shipper = shipper.Name

	h.handlerResponse(ctx, "create Income resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete income godoc
// @ID delete-income
// @Router /income/{id} [DELETE]
// @Summary Delete Income
// @Description Delete Income
// @Tags Income
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) IncomeDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.IncomeService().Delete(c.Request.Context(), &storage_service.IncomePrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.IncomeService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Income resposne", http.StatusNoContent, resp)
}
