package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/cashbox_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create shift godoc
// @ID create-shift
// @Router /shift [POST]
// @Summary Create Shift
// @Description Create Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param Shift body cashbox_service.ShiftCreate true "CreateShiftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftCreate(ctx *gin.Context) {
	var Shift cashbox_service.ShiftCreate

	err := ctx.ShouldBind(&Shift)
	if err != nil {
		h.handlerResponse(ctx, "CreateShift", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ShiftService().Create(ctx, &Shift)
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Shift resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID shift godoc
// @ID get-by-id-shift
// @Router /shift/{id} [GET]
// @Summary Get By ID Shift
// @Description Get By ID Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftGetById(ctx *gin.Context) {
	ShiftId := ctx.Param("id")

	if !helper.IsValidUUID(ShiftId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.ShiftService().GetById(ctx, &cashbox_service.ShiftPrimaryKey{Id: ShiftId})
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Shift resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList shift godoc
// @ID get-list-shift
// @Router /shift [GET]
// @Summary Get List Shift
// @Description Get List Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-shift-id query string false "search-shift-id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Shift offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Shift limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.ShiftService().GetList(ctx.Request.Context(), &cashbox_service.ShiftGetListRequest{
		Offset:        int64(offset),
		Limit:         int64(limit),
		SearchShiftId: ctx.Query("search-shift-id"),
	})
	if err != nil {
		h.handlerResponse(ctx, "ShiftService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Shift resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update shift godoc
// @ID update-shift
// @Router /shift/{id} [PUT]
// @Summary Update Shift
// @Description Update Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Shift body cashbox_service.ShiftUpdate true "UpdateShiftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftUpdate(ctx *gin.Context) {
	var (
		id          string = ctx.Param("id")
		Shiftupdate cashbox_service.ShiftUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Shiftupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Shift should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Shiftupdate.Id = id
	resp, err := h.services.ShiftService().Update(ctx.Request.Context(), &Shiftupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.ShiftService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Shift resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete shift godoc
// @ID delete-shift
// @Router /shift/{id} [DELETE]
// @Summary Delete Shift
// @Description Delete Shift
// @Tags Shift
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) ShiftDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.ShiftService().Delete(c.Request.Context(), &cashbox_service.ShiftPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.ShiftService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Shift resposne", http.StatusNoContent, resp)
}
