package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
)

// @Security ApiKeyAuth
// Make_Income godoc
// @ID Make_Income
// @Router /make-income [POST]
// @Summary Make_Income
// @Description Make_Income
// @Tags Make_Income
// @Accept json
// @Procedure json
// @Param MakeIncome body storage_service.IncomePrimaryKey true "MakeIncomeRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) MakeIncome(ctx *gin.Context) {
	var IncomeId storage_service.IncomePrimaryKey

	err := ctx.ShouldBind(&IncomeId)
	if err != nil {
		h.handlerResponse(ctx, "MakeIncome", http.StatusBadRequest, err.Error())
		return
	}
	resp, err := h.services.IncomeService().DoIncomes(ctx, &storage_service.IncomePrimaryKey{Id: IncomeId.Id})
	if err != nil {
		h.handlerResponse(ctx, "IncomeService().DoIncomes", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "Do Income resposne", http.StatusNoContent, resp)
}
