package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/market_go/go_api_gateway/genproto/storage_service"
	"gitlab.com/market_go/go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Create left godoc
// @ID create-left
// @Router /left [POST]
// @Summary Create Left
// @Description Create Left
// @Tags Left
// @Accept json
// @Procedure json
// @Param Left body storage_service.LeftCreate true "CreateLeftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) LeftCreate(ctx *gin.Context) {
	var Left storage_service.LeftCreate

	err := ctx.ShouldBind(&Left)
	if err != nil {
		h.handlerResponse(ctx, "CreateLeft", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.LeftService().Create(ctx, &Left)
	if err != nil {
		h.handlerResponse(ctx, "LeftService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create Left resposne", http.StatusCreated, resp)
}

// @Security ApiKeyAuth
// GetByID left godoc
// @ID get-by-id-left
// @Router /left/{id} [GET]
// @Summary Get By ID Left
// @Description Get By ID Left
// @Tags Left
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) LeftGetById(ctx *gin.Context) {
	LeftId := ctx.Param("id")

	if !helper.IsValidUUID(LeftId) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "Invaid uuid")
		return
	}

	resp, err := h.services.LeftService().GetById(ctx, &storage_service.LeftPrimaryKey{Id: LeftId})
	if err != nil {
		h.handlerResponse(ctx, "LeftService().GetById", http.StatusBadRequest, err.Error())
		return
	}

	h.handlerResponse(ctx, "get by id Left resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// GetList left godoc
// @ID get-list-left
// @Router /left [GET]
// @Summary Get List Left
// @Description Get List Left
// @Tags Left
// @Accept json
// @Procedure json
// @Param offset query string false "offset"
// @Param limit query string false "limit"
// @Param search-by-branch query string false "Поиск по Филиалу"
// @Param search-by-brand query string false "Поиск по Бренду"
// @Param search-by-category query string false "Поиск по Категори"
// @Param search-by-barcode query string false "Поиск по Штрихкоду"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) LeftGetList(ctx *gin.Context) {
	offset, err := h.getOffsetQuery(ctx.Query("offset"))
	if err != nil {
		h.handlerResponse(ctx, "get list Left offset", http.StatusBadRequest, "invalid offset")
		return
	}

	limit, err := h.getLimitQuery(ctx.Query("limit"))
	if err != nil {
		h.handlerResponse(ctx, "get list Left limit", http.StatusBadRequest, "invalid limit")
		return
	}

	resp, err := h.services.LeftService().GetList(ctx.Request.Context(), &storage_service.LeftGetListRequest{
		Offset:           int64(offset),
		Limit:            int64(limit),
		SearchByBranch:   ctx.Query("search-by-branch"),
		SearchByBrand:    ctx.Query("search-by-brand"),
		SearchByCategory: ctx.Query("search-by-category"),
		SerachByBarcode:  ctx.Query("search-by-barcode"),
	})
	if err != nil {
		h.handlerResponse(ctx, "LeftService().GetList", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "get list Left resposne", http.StatusOK, resp)
}

// @Security ApiKeyAuth
// Update left godoc
// @ID update-left
// @Router /left/{id} [PUT]
// @Summary Update Left
// @Description Update Left
// @Tags Left
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Param Left body storage_service.LeftUpdate true "UpdateLeftRequest"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) LeftUpdate(ctx *gin.Context) {
	var (
		id         string = ctx.Param("id")
		Leftupdate storage_service.LeftUpdate
	)

	if !helper.IsValidUUID(id) {
		h.handlerResponse(ctx, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	err := ctx.ShouldBindJSON(&Leftupdate)
	if err != nil {
		h.handlerResponse(ctx, "error Left should bind json", http.StatusBadRequest, err.Error())
		return
	}

	Leftupdate.Id = id
	resp, err := h.services.LeftService().Update(ctx.Request.Context(), &Leftupdate)
	if err != nil {
		h.handlerResponse(ctx, "services.LeftService().Update", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(ctx, "create Left resposne", http.StatusAccepted, resp)
}

// @Security ApiKeyAuth
// Delete left godoc
// @ID delete-left
// @Router /left/{id} [DELETE]
// @Summary Delete Left
// @Description Delete Left
// @Tags Left
// @Accept json
// @Procedure json
// @Param id path string true "id"
// @Success 200 {object} Response{data=string} "Success Request"
// @Response 400 {object} Response{data=string} "Bad Request"
// @Failure 500 {object} Response{data=string} "Server error"
func (h *Handler) LeftDelete(c *gin.Context) {

	var id string = c.Param("id")

	if !helper.IsValidUUID(id) {
		h.handlerResponse(c, "is valid uuid", http.StatusBadRequest, "invalid id")
		return
	}

	resp, err := h.services.LeftService().Delete(c.Request.Context(), &storage_service.LeftPrimaryKey{Id: id})
	if err != nil {
		h.handlerResponse(c, "services.LeftService().Delete", http.StatusInternalServerError, err.Error())
		return
	}

	h.handlerResponse(c, "create Left resposne", http.StatusNoContent, resp)
}
